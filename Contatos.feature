# language: pt
# encoding:utf-8



Funcionalidade: Adicionar um contato no WhatsApp

Como usuário do WhatsApp 
Eu quero poder adicionar um contato 
para poder enviar mensagens

Cenário: Adicionando um contato

	Dado que estou na aba "Contatos"
	Quando pressionar o ícone "Adicionar contatos"
		E preencher os dados
		E pressionar o botão "OK"
	Então adicionar com sucesso o contato



Cenário: Buscar contato 

	Dado que estou na aba "Contatos"
	Quando pressionar o botão "Buscar"
		E digitar o nome do contato
	Então exibir o contato adicionado

