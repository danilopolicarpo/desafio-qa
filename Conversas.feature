# language: pt
# encoding:utf-8


Funcionalidade: Enviar mensagem para um contato no WhatsApp

Como usuário do WhatsApp 
Eu quero poder enviar mensagens, imagens e video
para meu contato 

Cenário: Usuário envia um texto para o contato

	Dado que estou na aba "Conversas"
	Quando pressiono um contato
		E digito o texto "Bom dia"
		E pressiono o botão "Enviar"
	Então o texto é enviado para o contato com sucesso



Cenário: Usuário envia uma imagem para o contato

	Dado que estou na aba "Conversas"
	Quando pressiono um contato
		E pressiono a opção de "fotos"
	Então exibir minha galeria com todas as fotos
	Quando seleciono uma foto
		E pressiono o botão "Enviar"
	Então a foto é enviada para o contato com sucesso



Cenário: Usuário envia um video para o contato

	Dado que estou na aba "Conversas"
	Quando pressiono um contato
		E pressiono a opção de "fotos"
	Então exibir minha galeria com todas as fotos
		E pressiono a opção todos os videos
	Quando seleciono um video
		E pressiono o botão "Enviar"
	Então o video é enviado para o contato com sucesso


	